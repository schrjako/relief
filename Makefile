%.pdf:%.tex relief.tex langs.tex
	latex -shell-escape -interaction=nonstopmode -output-format=pdf $<

default:
	@make relief_eng.pdf; make relief_slo.pdf

.PHONY: clean langs.tex relief.tex

clean:
	@rm *.dvi *.log *.lof *.aux *.out *.toc -fv
